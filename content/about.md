+++
title = 'About'
date = 2023-12-31T11:13:38-06:00
draft = false
tags = ['pages']
+++

This site is just a place for me to keep notes about things I want to remember
as well as a jumping off point for other things.
