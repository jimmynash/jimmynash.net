---
title: "Examples"
date: 2023-12-30T19:46:08-06:00
draft: false
toc: true
images:
tags:
  - testing
  - example
  - hugo
---
# Page of Examples

This page just contains examples of what I'm allowed to do with markup here.

## Section Title

These h2 tags show up as sections if the toc is turned on in the frontmatter.

## Code Highlighting Section

Some PHP code

``` php
if ($isathing) {
  echo('Seems to be a thing...');
}
```
